image: azul/zulu-openjdk-alpine:11

# Enable Docker-in-Docker (DinD) for TestContainers
services:
  - docker:dind

variables:
  DOCKER_HOST: "tcp://docker:2375"
  # Improve performance by using overlayfs
  DOCKER_DRIVER: overlay2

before_script:
  # Tell Gradle to write its files to the build directory
  - export GRADLE_USER_HOME=`pwd`/.gradle
  # Remove files that might be problematic (recommended by Travis CI)
  - rm -f  ${GRADLE_USER_HOME}/caches/modules-2/modules-2.lock
  - rm -fr ${GRADLE_USER_HOME}/caches/*/plugin-resolution/

# Some words on how caching works in GitLab CI:
#   Only sub-directories of the build directory can be added
#   to the cache. Expansion of variables does not work. We
#   will only keep the `wrapper` and `caches` sub-directories
#   of Gradle in the cache. Gradle will place those into
#   $HOME/.gradle by default. To be able to save them, they
#   need to be present in the build directory. By setting
#   `GRADLE_USER_HOME` we make Gradle place the directories
#   into a location from which we can move them to the cache
#   (and retrieve them from there again). Although the cache
#   directory is available as `/cache` you cannot directly
#   interact with it. Data will only be transferred to and
#   from the build directory. (It took me a while to get it
#   right…)
cache:
  key: gradle
  paths:
    # Gradle wrappers and downloaded artifacts
    - .gradle/wrapper
    - .gradle/caches

stages:
  - build
  - package
  - verify
  - deploy

build:
  stage: build
  script:
    - ./gradlew build
    - ./gradlew jacocoTestCoverageVerification
    - ./gradlew jacocoTestReport printCoverage
  coverage: '/^Coverage:\s(\d+\.\d+%)/'
  artifacts:
    when: always
    paths:
      - build/reports/
      - build/libs/orkg-*.jar
    reports:
      junit: build/test-results/**/TEST-*.xml

pages:
  stage: deploy
  script:
    - ./gradlew asciidoctor
    - mkdir -p public
    - cp -r build/docs/asciidoc/* public/
  artifacts:
    paths:
      - public
  only:
    - master

.docker-build-instructions: &docker-build-instructions
  image: gradle:6.3-jdk11
  stage: package
  dependencies:
    - build
  script:
    # Build using Jib (pushed automatically)
    - gradle jib -Djib.to.image=$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA -Djib.to.tags=$IMAGE_TAG -Djib.to.auth.username=$REGISTRY_USER -Djib.to.auth.password=$REGISTRY_PASSWORD

docker-build-master:
  variables:
    IMAGE_TAG: latest
  <<: *docker-build-instructions
  only:
    - master

docker-build:
  variables:
    IMAGE_TAG: $CI_COMMIT_REF_SLUG
  <<: *docker-build-instructions
  except:
    - master

# (Security) Scanning templates
include:
  - template: Container-Scanning.gitlab-ci.yml

container_scanning:
  stage: verify
  variables:
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE
